# API #

AvenueCode's Recruitment Java project

### Prerequisites ###

* Java (recommended: [Java 8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html))
* Tomcat (recommended: [Tomcat 6](https://tomcat.apache.org/download-60.cgi))

## Instructions ##

Go to the project folder and...

### Compiling ###

```
#!Maven

mvn package
```

### Installing dependencies ###


```
#!Maven

mvn install
```

### Running webapp ###


```
#!Maven

mvn jetty:run
```

### Running Tests ###

```
#!Maven

mvn test
```

### Cleaning project ###


```
#!Maven

mvn clean
```

## Endpoints ##

### GET ###

**Find Populated**

*/api/service/product/all*

Get all products including specified relationships

**Simple find**

*/api/service/product/allShort*

Get all products excluding relationships 

*The next two methods was created to complement finding object by another way instead of id*

## Find by text ##

*/api/service/product/search?keyword=value*

Search products by text full populated

## Find By text simple ##

*/api/service/product/searchShort?keyword=value*

Search products by text

## get Product Childs ##

*/api/service/product/getChilds?productId=value*

Get childs of productId

## Find images of product ##

*/api/service/product/getImages?productId=value*

Get images of productId

## Find product by id populated##

*/api/service/product/findPopulatedById?productId=value*

Get product by id populated with relationships

## Find product by id without relationships##

*/api/service/product/findById?productId=value*

Get images of productId

### POST ###

## Set product childs##

*/api/service/product/setChilds*

*Parameters*

Name        |Description
------------|-------------------------------
id          | Id of product to update
listChilds  | child's id array

Set childs of productId

## Set product images##

*/api/service/product/setImages*

*Parameters*

Name        |Description
------------|-------------------------------
id          | Id of product to update
listImages  | Image's id array

Set images of productId


