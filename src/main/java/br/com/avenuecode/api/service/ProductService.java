package br.com.avenuecode.api.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import br.com.avenuecode.api.dao.ProductDAO;
import br.com.avenuecode.api.model.Image;
import br.com.avenuecode.api.model.Product;
import br.com.avenuecode.api.util.ResponseEnum;

@Path("/product")
public class ProductService {
	
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllProducts(){
		ProductDAO productDAO = new ProductDAO();
		List<Product> list = productDAO.findAll();
		
		return Response.status(200).entity(list).build();
	}
	
	@GET
	@Path("/allShort")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllProductsShort(){
		ProductDAO productDAO = new ProductDAO();
		List<Product> list = productDAO.findAllShort();
		
		return Response.status(200).entity(list).build();
	}
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchProducts(@QueryParam("keyword") String keyWord){
		ProductDAO productDAO = new ProductDAO();
		List<Product> list = productDAO.searchAll(keyWord);
		
		return Response.status(200).entity(list).build();
	}
	
	@GET
	@Path("/searchShort")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchShortProducts(@QueryParam("keyword") String keyWord){
		ProductDAO productDAO = new ProductDAO();
		List<Product> list = productDAO.searchAllShort(keyWord);
		
		return Response.status(200).entity(list).build();
	}
	
	@GET
	@Path("/getChilds")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCHildsProduct(@QueryParam("productId") String id){
		ProductDAO productDAO = new ProductDAO();
		List<Product> list = productDAO.getChilds(Long.parseLong(id));
		
		return Response.status(200).entity(list).build();
	}
	
	@GET
	@Path("/getImages")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getImagesProduct(@QueryParam("productId") String id){
		ProductDAO productDAO = new ProductDAO();
		List<Image> list = productDAO.getImages(Long.parseLong(id));
		
		return Response.status(200).entity(list).build();
	}
	
	@GET
	@Path("/findPopulatedById")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductPopulated(@QueryParam("productId") String id){
		ProductDAO productDAO = new ProductDAO();
		
		return Response.status(200)
				.entity(productDAO.findPopulatedById(Long.parseLong(id))).build();
	}
	
	@GET
	@Path("/findById")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProduct(@QueryParam("productId") String id){
		ProductDAO productDAO = new ProductDAO();
		
		return Response.status(200)
				.entity(productDAO.findShortById(Long.parseLong(id))).build();
	}
	
	@POST
	@Path("/setChilds")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setChildsProduct(MultivaluedMap<String, String> params) throws JSONException{
		
		ProductDAO productDAO = new ProductDAO();
		ResponseEnum response = productDAO
				.setChilds(Long.parseLong(params.getFirst("id")), 
						params.getFirst("listChilds").split(","));
		
		JSONObject json = new JSONObject();
		
		if(response == ResponseEnum.SUCCESS){
			json.put("message", "Success on set childs");
			return Response.status(200).entity(json).build();
		}else{
			json.put("message", "Error on set childs");
			return Response.status(500).entity("Error on set childs").build();
		}
		
	}
	
	@POST
	@Path("/setImages")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response setImagesProduct(MultivaluedMap<String, String> params) throws JSONException{
		
		ProductDAO productDAO = new ProductDAO();
		ResponseEnum response = productDAO
				.setImages(Long.parseLong(params.getFirst("id")), 
						params.getFirst("listImages").split(","));
		
		JSONObject json = new JSONObject();
		
		if(response == ResponseEnum.SUCCESS){
			json.put("message", "Success on set images");
			return Response.status(200).entity(json).build();
		}else{
			json.put("message", "Error on set images");
			return Response.status(500).entity("Error on set childs").build();
		}
		
	}
	
}
