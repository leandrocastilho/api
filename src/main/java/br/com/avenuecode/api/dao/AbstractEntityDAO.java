package br.com.avenuecode.api.dao;

import br.com.avenuecode.api.driverManager.DriverManagerH2;

public abstract class AbstractEntityDAO {
	
	protected DriverManagerH2 driverManagerH2;
	
	public AbstractEntityDAO() {
		if (this.driverManagerH2 == null) {
			this.driverManagerH2 = new DriverManagerH2();
		}
	}

}
