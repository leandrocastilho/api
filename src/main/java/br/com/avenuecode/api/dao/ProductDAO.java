package br.com.avenuecode.api.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.avenuecode.api.model.Image;
import br.com.avenuecode.api.model.Product;
import br.com.avenuecode.api.util.ResponseEnum;

public class ProductDAO extends AbstractEntityDAO{

	private Root<Product> rootProduct;
	private CriteriaQuery<Product> query;

	public ProductDAO() {
		super();
		
		this.query = driverManagerH2.getBuilder().createQuery(Product.class);			
		this.rootProduct = query.from(Product.class);
	}

	public List<Product> findAll() {
		
		this.query.select(rootProduct).distinct(true);
		
		this.rootProduct.join("images", JoinType.LEFT);

		return this.driverManagerH2.getManager()
				.createQuery(this.query).getResultList();
	}

	public List<Product> findAllShort() {

		this.query.multiselect(
				this.rootProduct.get("id").as(Long.class), 
				this.rootProduct.get("name"), 
				this.rootProduct.get("description"));

		return this.driverManagerH2.getManager()
				.createQuery(this.query).getResultList();
	}

	public List<Product> searchAll(String keyWord) {	
		
		this.rootProduct.join("images", JoinType.LEFT);
		
		TypedQuery<Product> searchAllQuery = this.driverManagerH2.getManager()
				.createQuery(this.query.select(this.rootProduct)
						.where(buildPredicate(keyWord)).distinct(true));		
		
		return searchAllQuery.getResultList();
	}

	public List<Product> searchAllShort(String keyWord) {
		
		this.query.multiselect(
				this.rootProduct.get("id").as(Long.class),
				this.rootProduct.get("name"), 
				this.rootProduct.get("description"))
		.where(buildPredicate(keyWord));
		
		TypedQuery<Product> searchAllShortQuery = 
				this.driverManagerH2.getManager().createQuery(this.query);
		
		return searchAllShortQuery.getResultList();
	}
	
	public ResponseEnum setChilds(Long id, String[] listChilds){

		Product product = findById(id);
		
		this.driverManagerH2.getManager().getTransaction().begin();
		
		for(String childId : listChilds){
			
			Product child = findById(Long.parseLong(childId));
			child.setParentProduct(product);
			
			
			this.driverManagerH2.getManager().persist(child);			
		}
		
		this.driverManagerH2.getManager().getTransaction().commit();		
		
		return ResponseEnum.SUCCESS;
	}
	
	public List<Product>  getChilds(Long id){
		
		Product product = findById(id);
		
		this.query.multiselect(
				this.rootProduct.get("id").as(Long.class), 
				this.rootProduct.get("name"), 
				this.rootProduct.get("description"))
		.where(this.driverManagerH2.getBuilder().equal(this.rootProduct.get("parentProduct")
				.as(Product.class), product));
		
		return this.driverManagerH2.getManager().createQuery(this.query).getResultList();
		
	}
	
	public List<Image> getImages(Long id){
		
		this.rootProduct.join("images", JoinType.LEFT);
		
		Product product = findById(id);
		
		return product.getImages();
	}
	
	public ResponseEnum setImages(Long id, String[] listImages){
		Product product = findById(id);
		
		ImageDAO imageDAO = new ImageDAO();
		
		imageDAO.getDriverManager().getManager().getTransaction().begin();
		
		for(String imageId : listImages){
			Image image = imageDAO.findById(Long.parseLong(imageId));
			image.setProduct(product);
			
			imageDAO.getDriverManager().getManager().merge(image);		
		}
		
		imageDAO.getDriverManager().getManager().getTransaction().commit();		
		
		return ResponseEnum.SUCCESS;
	}
	
	public Product findById(Long id){
		this.query.select(rootProduct)
		.where(this.driverManagerH2.getBuilder()
				.equal(this.rootProduct.get("id").as(Long.class), id));
		
		return this.driverManagerH2.getManager().createQuery(this.query).getSingleResult();
	}
	
	public Product findPopulatedById(Long id){
		this.rootProduct.join("images", JoinType.LEFT);		
		
		return findById(id);
	}
	
	public Product findShortById(Long id){
		this.query.multiselect(
				this.rootProduct.get("id").as(Long.class), 
				this.rootProduct.get("name"), 
				this.rootProduct.get("description"))
		.where(this.driverManagerH2.getBuilder()
				.equal(this.rootProduct.get("id").as(Long.class), id));
		
		return this.driverManagerH2.getManager().createQuery(this.query).getSingleResult();
	}
	
	private Predicate buildPredicate(String keyWord){
		Predicate predicate = this.driverManagerH2.getBuilder().or();
		
		predicate = this.driverManagerH2.getBuilder().or(predicate, 
				this.driverManagerH2.getBuilder()
				.like(this.rootProduct.<String>get("name"), "%" + keyWord + "%"));
		
		predicate = this.driverManagerH2.getBuilder().or(predicate,
				this.driverManagerH2.getBuilder()
				.like(this.rootProduct.<String>get("description"), "%" + keyWord + "%"));
		
		return predicate;
	}

}
