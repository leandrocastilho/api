package br.com.avenuecode.api.dao;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.avenuecode.api.driverManager.DriverManagerH2;
import br.com.avenuecode.api.model.Image;

public class ImageDAO extends AbstractEntityDAO{
	
	private Root<Image> rootImage;
	private CriteriaQuery<Image> query;
	
	public ImageDAO() {
		super();
		
		this.query = driverManagerH2.getBuilder().createQuery(Image.class);			
		this.rootImage = query.from(Image.class);
	}
	
	public DriverManagerH2 getDriverManager(){
		return this.driverManagerH2;
	}

	public Image findById(Long id) {
		this.query.select(this.rootImage)
		.where(this.driverManagerH2.getBuilder()
				.equal(this.rootImage.get("id").as(Long.class), id));
		
		return this.driverManagerH2.getManager().createQuery(this.query).getSingleResult();
	}
	

}
