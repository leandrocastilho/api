package br.com.avenuecode.api.driverManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;

public class DriverManagerH2 extends DriverManager{
	
	private EntityManagerFactory factory;
	private EntityManager manager;
	private CriteriaBuilder builder;
	
	public DriverManagerH2() {
		bootstrapConfiguration();
	}

	@Override
	protected void bootstrapConfiguration() {
		this.factory = Persistence.createEntityManagerFactory("api");
		this.manager = factory.createEntityManager();		
		this.builder = this.manager.getCriteriaBuilder();		
	}

	public EntityManager getManager() {
		return manager;
	}

	public CriteriaBuilder getBuilder() {
		return builder;
	}	

}
