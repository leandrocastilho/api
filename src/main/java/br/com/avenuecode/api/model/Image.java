package br.com.avenuecode.api.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="images")
@XmlRootElement()
public class Image {
	
	public Image() {
	}
	
	public Image(Long id, String type){
		this.id = id;
		this.type = type;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Column
	private String type;
	
	@ManyToOne(cascade=CascadeType.MERGE)
	private Product product;
	
	public Long getId() {
		return id;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public String getType(){
		return this.type;
	}
	
	public void setProduct(Product product){
		this.product = product;
	}
	
}
