package br.com.avenuecode.api.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@Entity
@Table(name="products")
@XmlRootElement()
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
public class Product {
	
	public Product() {
	}
	
	public Product(Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Column
	private String name;
	
	@Column
	private String description;
	
	@ManyToOne
	@JoinColumn(name="parent_product_id")
	private Product parentProduct;
	
	@OneToMany(mappedBy="product", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, targetEntity=Image.class)
	@Column(name="images")
	private List<Image> images = new ArrayList<Image>();
	
	public Long getId() {
		return id;
	}

	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	public void setParentProduct(Product parentProduct){
		this.parentProduct = parentProduct;
	}
	
	public Product getParentProduct(){
		return this.parentProduct;
	}
	
	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}	

}
