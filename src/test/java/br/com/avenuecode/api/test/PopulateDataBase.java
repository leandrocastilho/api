package br.com.avenuecode.api.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.avenuecode.api.model.Image;
import br.com.avenuecode.api.model.Product;

public class PopulateDataBase {
	
	public static void main(String[] args) {
		
		//Product 1
		
		Product product = new Product();
		
		product.setName("Product 1");
		product.setDescription("First product registered");
		
		//Product 2
		
		Product product2 = new Product();
		
		product2.setName("Product 2");
		product2.setDescription("Second product registered");
		
		product2.setParentProduct(product);
		
		//Product 3
		
		Product product3 = new Product();
		
		product3.setName("Product 3");
		product3.setDescription("Third Product registered");
		
		product3.setParentProduct(product2);
		
		//Image 1 product 1
		Image image = new Image();
		
		image.setProduct(product);
		image.setType("jpg");
		
		//Image 2 product 1
		Image image2 = new Image();
		
		image2.setProduct(product);
		image2.setType("jpg");
		
		//Image 3 product 1
		Image image3 = new Image();
		
		image3.setProduct(product);
		image3.setType("jpg");
		
		//Image 1 product 2
		Image image4 = new Image();
		
		image4.setProduct(product2);
		image4.setType("jpg");
		
		//Image 2 product 2
		Image image5 = new Image();
		
		image5.setProduct(product2);
		image5.setType("jpg");
		
		//Image 3 product 2
		Image image6 = new Image();
		
		image6.setProduct(product2);
		image6.setType("jpg");
		
		//Image 1 product 3
		Image image7 = new Image();
		
		image7.setProduct(product3);
		image7.setType("jpg");
		
		//Image 2 product 2
		Image image8 = new Image();
		
		image8.setProduct(product3);
		image8.setType("jpg");
		
		//Image 3 product 2
		Image image9 = new Image();
		
		image9.setProduct(product3);
		image9.setType("jpg");
		
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("api");
		EntityManager manager = factory.createEntityManager();
		
		manager.getTransaction().begin();
		manager.persist(product);
		manager.persist(product2);
		manager.persist(product3);
		manager.persist(image);
		manager.persist(image2);
		manager.persist(image3);
		manager.persist(image4);
		manager.persist(image5);
		manager.persist(image6);
		manager.persist(image7);
		manager.persist(image8);
		manager.persist(image9);
		manager.getTransaction().commit();		
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Product> query = builder.createQuery(Product.class);
		Root<Product> rootProduct = query.from(Product.class);
		CriteriaQuery<Product> queryAll = query.select(rootProduct);
		TypedQuery<Product> all = manager.createQuery(queryAll);
		
		List<Product> products = all.getResultList();
		
		System.out.println(products);
		
	}
}
