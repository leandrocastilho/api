package br.com.avenuecode.api.test;

import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Assert;

public class ApiTest {
	
	@Test
	public void testDatabaseConnection(){
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("api");
		EntityManager manager = factory.createEntityManager();
		
		Assert.assertEquals(true, manager.isOpen());
	}
}
